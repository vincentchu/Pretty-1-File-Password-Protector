<?php
	$url = "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"; 
	$escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' );
    
	//Login credentials
    $LOGIN_INFORMATION = array(
      'admin' => 'myswatllc'
    );
    // request login? true - show login and password boxes, false - password box only
    define('USE_USERNAME', false);
    // User will be redirected to this page after logout
    define('LOGOUT_URL', 'https://' . $_SERVER['HTTP_HOST']);
    // This parameter is only useful when TIMEOUT_MINUTES is not zero
    // true - timeout time from last activity, false - timeout time from login
    define('TIMEOUT_CHECK_ACTIVITY', false);
    //Theme Settings.
    //Background Image
    define('BG' , "https://source.unsplash.com/random/1920x1080");
    //Button Text Color 
    define('BTN_TEXT' , "#fff");
    //Button Color
    define('BTN_COLOR' , "#007bff");
    //Button Hover
    define('BTN_HOVER' , "#0069d9");
    //Button Active
    define('BTN_ACTIVE' , "#0062cc");
    ##################################################################
    #  SETTINGS END
    ##################################################################
    ///////////////////////////////////////////////////////
    // do not change code below
    ///////////////////////////////////////////////////////
    
    if (isset($_POST['remember'])) {
    	$TIMEOUT_MINUTES = 20160; 
   	} else {
   		$TIMEOUT_MINUTES = 0;
    }

    // timeout in seconds
    $timeout = ($TIMEOUT_MINUTES == 0 ? 0 : time() + $TIMEOUT_MINUTES * 60);
    // logout?
    if(isset($_GET['logout'])) {
      setcookie("verify", '', $timeout, '/'); // clear password;
      header('Location: ' . LOGOUT_URL);
      exit();
    }
    if(!function_exists('showLoginPasswordProtect')) {
    // show login form
    function showLoginPasswordProtect($error_msg) {
    ?>


	<!DOCTYPE html>

	<head>
		<title>Login Page</title>
		<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
		<META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
		<link rel="shortcut icon" href="favicon.ico" />
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha256-YLGeXaapI0/5IgZopewRJcFXomhRMlYYjugPLSyNjTY=" crossorigin="anonymous" />

		<style>
			body {
				background-image: url(<?php echo BG; ?>);
				min-height: 100%;
			}

			.session_login .form-signin .form-group.form-signin-group .btn {
				width: 100%;
				margin-bottom: 1px;
			}

			.form-container {
				position: relative;
				max-width: 400px;
				margin: 5% auto 0;
				padding: 30px 30px 10px 30px;
				color: #444;
				border: 1px solid #ddd;
				background-color: #fff;
				-webkit-box-shadow: 1px 1px 7px rgba(0, 0, 0, .1);
				box-shadow: 1px 1px 7px rgba(0, 0, 0, .1);
				border-radius: 5px;
			}

			@media (max-width: 500px) {
				.form-container {
					max-width: 90%;
				}
			}

			.btn.full-width {
				width: 100%;
				background-color: <?php echo BTN_COLOR; ?> !important;
				color: <?php echo BTN_TEXT; ?> !important;
				text-shadow: #000 1px 2px 0.5em;
				border-color: transparent;
			}

			.btn.full-width:hover {
				background-color: <?php echo BTN_HOVER; ?> !important;
				border-color: transparent;
			}

			.btn.full-width:active {
				background-color: <?php echo BTN_ACTIVE; ?> !important;
				border-color: transparent;
			}

			.login-message {
				font-size: 2em;
			}

			.session_login .form-signin-paragraph {
				margin-top: 3px;
				margin-bottom: 7px;
			}

		</style>

	</head>


	<body>
		
	<div class="form-container">
      <form method="post">
        <h2 class="text-center">Login to Access</h2>
        <p class="text-truncate text-center"><strong><em> <?php $url =  "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"; $escaped_url = htmlspecialchars( $url, ENT_QUOTES, 'UTF-8' ); echo $escaped_url;?></em></strong>
          <span class="text-danger">
            <strong><br /><?php echo $error_msg; ?></strong>
          </span>
          <?php if (USE_USERNAME) echo 
    
          '<div class="form-group">
           <div class="input-group"><div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-user"></i></div></div><input type="input" class="form-control" placeholder="Username" name="access_login" required /></div>
          </div>'; 
          
          ?>
          </p>
			<div class="form-group">
				<div class="input-group">
					<div class="input-group-prepend">
						<div class="input-group-text"><i class="fas fa-lock"></i></div>
					</div>
					<input type="password" class="form-control" placeholder="Password" name="access_password" required />
				</div>
			</div>
            <div class="form-group form-check">
		    	<input type="checkbox" class="form-check-input" id="remember" name="remember">
		    	<label class="form-check-label" for="remember">Remember Me for 2 Weeks.</label>
  			</div>

          <button type="submit" class="btn full-width" name="Submit">
          <i class="fas fa-sign-in-alt"></i> Sign In
        </button>
      </form>
      <br />
    </div>

	<script async src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/js/all.min.js" integrity="sha256-HT9Zb3b1PVPvfLH/7/1veRtUvWObQuTyPn8tezb5HEg=" crossorigin="anonymous"></script>

	</body>

	</html>



	<?php
      // stop at this point
      die();
    }
    }
    // user provided password
    if (isset($_POST['access_password'])) {
      $login = isset($_POST['access_login']) ? $_POST['access_login'] : '';
      $pass = $_POST['access_password'];
      if (!USE_USERNAME && !in_array($pass, $LOGIN_INFORMATION)
      || (USE_USERNAME && ( !array_key_exists($login, $LOGIN_INFORMATION) || $LOGIN_INFORMATION[$login] != $pass ) ) 
      ) {
        showLoginPasswordProtect("Incorrect credentials.");
      }
      else {
        // set cookie if password was validated
        setcookie("verify", password_hash($login.'%'.$pass, PASSWORD_DEFAULT), $timeout, '/');
        
        // Some programs (like Form1 Bilder) check $_POST array to see if parameters passed
        // So need to clear password protector variables
        unset($_POST['access_login']);
        unset($_POST['access_password']);
        unset($_POST['Submit']);
      }
    }
    else {
      // check if password cookie is set
      if (!isset($_COOKIE['verify'])) {
        showLoginPasswordProtect("");
      }
      // check if cookie is good
      $found = false;
      foreach($LOGIN_INFORMATION as $key=>$val) {
        $lp = (USE_USERNAME ? $key : '') .'%'.$val;
        if ($_COOKIE['verify'] == password_verify($lp , $_COOKIE['verify'])) {
          $found = true;
          // prolong timeout
          if (TIMEOUT_CHECK_ACTIVITY) {
            setcookie("verify", password_verify($lp , $_COOKIE['verify']), $timeout, '/');
          }
          break;
        }
      }
      if (!$found) {
        showLoginPasswordProtect("");
      }
    }
    ?>
